# Projekt: PIS_SoSe19_Projekt_Sorting_Algorithms_Visualized Fr/1, Kr)

Name & Praktikumstermin : Geronimo Nassois, 5247321 (Fr/1, Kr)

# Inhaltsverzeichnis
- [Projekt: PIS_SoSe19_Projekt_Sorting_Algorithms_Visualized Fr/1, Kr)](#Projekt-PISSoSe19ProjektSortingAlgorithmsVisualized-Fr1-Kr)
- [Inhaltsverzeichnis](#Inhaltsverzeichnis)
  - [Kurzbeschreibung des Projekts](#Kurzbeschreibung-des-Projekts)
  - [Beschreibung des Projektaufbaus](#Beschreibung-des-Projektaufbaus)
    - [Darstellung](#Darstellung)
    - [Datenmodell](#Datenmodell)
    - [Ablaufsteuerung](#Ablaufsteuerung)
    - [Abgabedateien (LOC)](#Abgabedateien-LOC)
    - [Testdateien (TST)](#Testdateien-TST)
  - [Aufbau der Anwendung](#Aufbau-der-Anwendung)
      - [Backend](#Backend)
      - [nassois.geronimo](#nassoisgeronimo)
        - [nassois.geronimo.algorithms](#nassoisgeronimoalgorithms)
        - [nassois.geronimo.config](#nassoisgeronimoconfig)
        - [nassois.geronimo.listener](#nassoisgeronimolistener)
        - [nassois.geronimo.utils](#nassoisgeronimoutils)
        - [nassois.geronimo.web](#nassoisgeronimoweb)
        - [nassois.geronimo.web.controllers](#nassoisgeronimowebcontrollers)
      - [Frontend / Client](#Frontend--Client)
        - [public/](#public)
        - [public/css](#publiccss)
        - [public/js](#publicjs)
      - [Tests](#Tests)
  - [Technischer Anspruch (TA) und Umsetzung der Features](#Technischer-Anspruch-TA-und-Umsetzung-der-Features)
    - [Dokumentation der Features](#Dokumentation-der-Features)
      - [1. Externe Bibliotheken](#1-Externe-Bibliotheken)
        - [1. p5.js](#1-p5js)
        - [2. Bootstrap 4](#2-Bootstrap-4)
        - [3. JQuery](#3-JQuery)
      - [2. Streams](#2-Streams)
      - [3. JSON](#3-JSON)
  - [Quellennachweis](#Quellennachweis)

## Kurzbeschreibung des Projekts

> Dieses Projekt dient als Klausurleistung für das Modul "Programmierung interaktiver Systeme" [Modul-Nr: 1016].
Das Projekt dient zur Visualisierung von Sortieralgorithmen.
Die Anwendung ermöglicht es dem User mehrere Parameter zu setzten, beispielsweise die Anzahl der Elemente der zu sortierende ArrayList, den anzuwendenen Algorithmus und die Geschwindigkeit der Darstellung.
Nach der Auswahl der Parameter und der Bestätigung durch den Butto "Submit" beginnt die Visualisierung des Algorithmus mit der Darstellung der unsortierten und zufällig generierten ArrayList.
Die Visualisierung setzt sich mit der Darstellung der jeweiligen Schritten des Sortieralgorithmus fort und endet mit der Darstellung der sortierten ArrayList.
Die einzelnen Iterationsschritte, d.h. die Vergleiche und das Vertauschen der jeweiligen Elemente stellt sich durch farblich markierte Elemente im dargestellten Iterationsschritt dar.
Das zu sortierende Element wir in Grün und das zu vergleichende Element in Rot dargestellt.
Zudem werden die Anzahl der Vergleiche und Tausch-Operationen angezeigt.


![GitHub Logo](./img/index_Mockup.png)
*Concept Mock-Up*


![GitHub Logo](./img/App.png)
*Application*


**Hinweise**: keine

## Beschreibung des Projektaufbaus

Das Projekt implementiert das Software-Architektur-Modell des Model-View-Controllers.
Die Darstellung, das Datenmodell und die Ablaufsteuerung sind in verschiendene Komponenten ausgelagert.

### Darstellung

[/src/main/resource/](/src/main/resources)

### Datenmodell

[/src/main/java/nassois/geronimo/algorithms/](/src/main/java/nassois/geronimo/algorithms)

[/src/main/java/nassois/geronimo/listener/](/src/main/java/nassois/geronimo/listener)

[/src/main/java/nassois/geronimo/utils/](/src/main/java/nassois/geronimo/utils)

### Ablaufsteuerung

[/src/main/java/nassois/geronimo/web/Router.java](/src/main/java/nassois/geronimo/web/Router.java)

[/src/main/java/nassois/geronimo/web/controllers](/src/main/java/nassois/geronimo/web/controllers)


### Abgabedateien (LOC)

Verlinkter Dateiname | Dateiart | LOC
---------------------|----------|-----
[App.java](/src/main/java/nassois/geronimo/App.java) | Java | 8
[BubbleSort.java](/src/main/java/nassois/geronimo/algorithms/BubbleSort.java) | Java | 44
[InsertionSort.java](/src/main/java/nassois/geronimo/algorithms/InsertionSort.java) | Java | 43
[Quicksort.java](/src/main/java/nassois/geronimo/algorithms/QuickSort.java) | Java | 66
[Selectionsort.java](/src/main/java/nassois/geronimo/algorithms/QuickSort.java) | Java | 45
[AppConfig.java](/src/main/java/nassois/geronimo/config/AppConfig.java) | Java | 23
[IterationTuple.java](/src/main/java/nassois/geronimo/listener/IterationTuple.java) | Java | 23
[SortingAlgorithmVisualizer.java](/src/main/java/nassois/geronimo/listener/SortingAlgorithmListener.java) | Java | 85
[JSONParser.java](/src/main/java/nassois/geronimo/utils/JSONParser.java) | Java | 36
[RandomListGenerator.java](/src/main/java/nassois/geronimo/utils/RandomListGenerator.java) | Java | 20
[AlgorithmController.java](/src/main/java/nassois/geronimo/listener/SortingAlgorithmListener.java) | Java | 64
[Router.java](/src/main/java/nassois/geronimo/web/Router.java) | Java | 24
[sketch.js](/src/main/resources/public/js/sketch.js) | JavaScript | 81
[index.html](/src/main/resources/public/index.html) | HTML | 108
[style.css](/src/main/resources/public/css/style.css) | CSS | 50


### Testdateien (TST)

Verlinkter Dateiname | Testart | Anzahl der Tests
---------------------|---------|-----------------
[BubbleSortTest.java](/src/test/java/BubbleSortTest.java) | JUnit5 | 5
[InsertionSortTest.java](/src/test/java/InsertionSortTest.java) | JUnit5 | 5
[IterationTupleTest.java](/src/test/java/IterationTupleTest.java) | JUnit5 | 5
[QuickSortTest.java](/src/test/java/QuickSortTest.java) | JUnit5 | 5
[SelectionSortTest.java](/src/test/java/SelectionSortTest.java) | JUnit5 | 5
[SortingAlgorithmListenerTest.java](/src/test/java/SortingAlgorithmListenerTest.java) | JUnit5 | 5

Die Tests werden wie folgt ausgeführt:

Mit dem Kommando ```gradle clean test```!

## Aufbau der Anwendung

Projektstruktur:

```
src
├── main
│   ├── java
│   │   └── nassois
│   │       └── geronimo
│   │           ├── algorithms
│   │           │   ├── BubbleSort.java
│   │           │   ├── InsertionSort.java
│   │           │   ├── Quicksort.java
│   │           │   └── SelectionSort.java
│   │           ├── App.java
│   │           ├── config
│   │           │   └── AppConfig.java
│   │           ├── listener
│   │           │   ├── IterationTuple.java
│   │           │   └── SortingAlgorithmListener.java
│   │           ├── utils
│   │           │   ├── info
│   │           │   │   └── bubblesort
│   │           │   ├── JSONParser.java
│   │           │   └── RandomListGenerator.java
│   │           └── web
│   │               ├── controllers
│   │               │   └── AlgorithmController.java
│   │               └── Router.java
│   └── resources
│       └── public
│           ├── css
│           │   └── style.css
│           ├── index.html
│           └── js
│               └── sketch.js
└── test
    └── java
        ├── BubbleSortTest.java
        ├── InsertionSortTest.java
        ├── IterationTupleTest.java
        ├── QuickSortTest.java
        ├── SelectionSortTest.java
        └── SortingAlgorithmListenerTest.java

17 directories, 22 files

```

Die Projektstruktur gliedert sich folgendermaßen auf:

#### Backend 
[/src/main/java/](/src/main/java/)

#### [nassois.geronimo](/src/main/java/nassois/geronimo/)

> Dies ist das Base-Package in welchem der gesamte Sourc-Code für die Anwendung liegt.

* [App.java](/src/main/java/nassois/geronimo/App.java)
(*Starts Javalin-Server*)


##### [nassois.geronimo.algorithms](/src/main/java/nassois/geronimo/algorithms)

* [BubbleSort.java](/src/main/java/nassois/geronimo/algorithms/BubbleSort.java)
(*BubbleSort-Algoritmus*)

* [InsertionSort.java](/src/main/java/nassois/geronimo/algorithms/InsertionSort.java)
(*InsertionSort-Algoritmus*)

* [QuickSort.java](/src/main/java/nassois/geronimo/algorithms/QuickSort.java)
(*QuickSort-Algoritmus*)

* [SelectionSort.java](/src/main/java/nassois/geronimo/algorithms/SelectionSort.java)
(*SelectionSort-Algoritmus*)


##### [nassois.geronimo.config](/src/main/java/nassois/geronimo/config)

* [AppConfig.java](/src/main/java/nassois/geronimo/config/AppConfig.java) (*Konfiguration des Servers*)

##### [nassois.geronimo.listener](/src/main/java/nassois/geronimo/listener)

* [SortingAlgorithmListener.java](/src/main/java/nassois/geronimo/listener/SortingAlgorithmListener.java) (*Speicherung der Meta-Daten des jeweiligen Sortieralgorithmus* (*Sortiervorgangs*))

##### [nassois.geronimo.utils](/src/main/java/nassois/geronimo/utils)

>Helfer-Werkzeuge:
* [JSONParser.java](/src/main/java/nassois/geronimo/utils/JSONParser.java) 
(*konvertiert Sortier-Algorithmus-Meta-Daten zu JSON*)

* [RandomListGenerator.java](/src/main/java/nassois/geronimo/utils/RandomListGenerator.java) (*generiert eine zufällige Liste mit gegebener Größe*)

##### [nassois.geronimo.web](/src/main/java/nassois/geronimo/web/)

* [Router.java](/src/main/java/nassois/geronimo/web/Router.java) (*Setzt die Routen des Servers*)

##### [nassois.geronimo.web.controllers](/src/main/java/nassois/geronimo/web/controllers)

* [AlgorithmController.java](/src/main/java/nassois/geronimo/web/controllers/AlgorithmController.java) (*Controller der die Pfade der ausgewählten Algorithmen handelt*)



#### Frontend / Client
[/src/main/resources/](/src/main/resources)
> Base-Directory für alle clientseitigen Berechnungen

##### [public/](/src/main/resources/public/css/)
* [index.html](/src/main/resources/public/index.html)
(*index.html*)

##### [public/css](/src/main/resources/public/css/)
* [style.css](/src/main/resources/public/css/style.css)
(*Style-Sheet für Darstellung*)

##### [public/js](/src/main/resources/public/js/)

* [sketch.js](/src/main/resources/public/js/sketch.js)
(*Javascript-Code*)

#### Tests 
[/src/test/java/](/src/test/java/)
> Base-Directory für JUnit Tests

* [BubbleSortTest.java](/src/test/java/BubbleSortTest.java) (*Bubble-Sort JUnit-Tests*)

* [InsertionSortTest.java](/src/test/java/InsertionSortTest.java) (*Bubble-Sort JUnit-Tests*)

* [IterationTupleTest.java](/src/test/java/IterationTupleTest.java) (*Iteration-Tuple JUnit-Tests*)

* [QuickSortTest.java](/src/test/java/QuickSortTest.java) (*Quick-Sort JUnit-Tests*)

* [SelectionSortTest.java](/src/test/java/SelectionSortTest.java) (*Selection-Sort JUnit-Tests*)

* [SortingAlgorithmListenerTest.java](/src/test/java/SortingAlgorithmListenerTest.java) (*Sorting-Algorithm-Listener JUnit-Tests*)



## Technischer Anspruch (TA) und Umsetzung der Features

Das Projekt beinhaltet die folgende Feature. Die verlinkte Datei zeigt beispielhaft den Einsatz dieses Features in den angegebenen Zeilen im Quellcode.

1. Nutzung einer externen Bibliothek für das Frontend aus HTML/CSS/JavaScript (Beispiel ist Bootstrap), [index.html](/src/main/resources/public/index.html) (53-56)
   
2. Streams, [SortingAlgorithmListener.java](src/main/java/nassois/geronimo/listener/SortingAlgorithmListener.java) (45,46,50),
[BubbleSort.java](/src/main/java/nassois/geronimo/algorithms/BubbleSort.java) (42),
[Insertionsort.java](/src/main/java/nassois/geronimo/algorithms/InsertionSort.java) (40)

3. JSON-Datenformat, [JSONParser.java](src/main/java/nassois/geronimo/utils/JSONParser.java) (11-36), [sketch.js](src/main/resources/public/js/sketch.js) (28)


### Dokumentation der Features
#### 1. Externe Bibliotheken

Das Projekt nutzt drei verschiedene externe Bibliotheken zur Darstellung, sowie zur Datenverarbeitung.
Die benutzten Bibliotheken sind:

1. [JQuery](https://jquery.com/)
2. [Bootstrap 4](https://getbootstrap.com/)
3. [p5.js](https://p5js.org)

##### 1.  p5.js
<img src="./img/giphy.gif" alt="drawing" style="width:450px;"/>

Die Nutzung der Bibliothek beschränkt sich auf die Darstellung des Sortieralgorithmus, da diese hauptsächlich zur Visualisierung geeignet ist.

p5.js wird in der Anwendung zur Darstellung der Sortieralgorithmen verwendet.
Durch die simpliziät der Bibliothek und die hervorragenden Beispiele und Tutorials ist es möglich schnell Visualisierungen in javascript/p5 zu realisieren.

Die Darstellung der Algorithmen erfolgt durch das Durchiterieren der JSON-ArrayListen ```'steps'``` & ```'iterations'```.
Es wird die erste Permutation (*unsortierte ArrayList*) des jeweiligen zufälligen generierten ArrayList gezeichnet.
Anschliessen wird durch ```'steps'``` iteriert.
Wenn das gezeichnete Element der ArrayList ein Element ist, welches in ```'steps'``` verglichen wird, ändert sich die Füllfarbe des jeweiligen Elements.
Es wird so lange durch ```'steps'``` iteriert, bis ein ```'steps'```-Element den Variablen-Wert ``` 'steps'.swapped == true ``` hat.
Dann wird das "Vertauschen" der Elemente dargestellt indem die nächste Permutation der ArrayList ```'iterations'``` gezeichnet wird.

![p5](./img/p5.png)

*die zwei wichtigsten p5-Funktionen in [sketch.js](src/main/resources/public/js/sketch.js)*

Die in diesem Projekt verwendeten p5-Mehtoden umfassen:

* [frameRate()](https://p5js.org/reference/#/p5/frameRate) 
>**Einstellung der Framerate der draw()-Funktion**
* [createCanvas()](https://p5js.org/reference/#/p5/createCanvas)
>**erstellt Canvas auf dem die Visualizierung realisiert wird**
* [background()](https://p5js.org/reference/#/p5/background)
>**ändert Hintergrund des Canvas**
* [fill()](https://p5js.org/reference/#/p5/fill)
>**Füllfarbe des Elements**
* [stroke()](https://p5js.org/reference/#/p5/stroke)
>**Randfarbe des Elements**
* [strokeWeight()](https://p5js.org/reference/#/p5/strokeWeight)
>**Dicke des Randes**
* [rect()](https://p5js.org/reference/#/p5/rect)
>**erstellt ein Rechteck auf dem Canvas**

##### 2. Bootstrap 4
<img src="./img/bootstrap_4.png" alt="drawing" style="width:300px;"/>

Die Bootstrap 4 Bibliothek findet ihren Nutzen im kompletten Aufbau der [index.html](https://git.thm.de/gnss19/pis_sose19_projekt_sorting_algorithms_visualized/blob/master/src/main/resources/public/index.html).
Das Projekt nutzt die Darstellungsmethoden der durch die Bibliothek gegebenen Containerfunktionalität sowie, in manchen Teilen, die Background-Klassen.

##### 3. JQuery
<img src="./img/jquery_logo.png" alt="drawing" style="width:300px;"/>

JQuery wird in diesem Projekt angewendet um gewisse JavaScript-Funktionen einfacher und kürzer zu gestalten.
Das clientseitige Abfangen der JSON-Daten zur Visualisierung des jeweiligen Algorithmus wird durch eine JQuery-Funktion realisiert ([sketch.js](src/main/resources/public/js/sketch.js)).

![JQuery](./img/jquery.png)

*JQuery Beispiel: Abfangen der vom Server gesendeten JSON-Daten in [sketch.js](src/main/resources/public/js/sketch.js)*

#### 2. Streams

In dem Projekt werden an mehereren Stellen Streams und Lambda-Ausdrücke verwendet (*Referenz in Auzählung*).

In den meisten Fällen wurden sie genutzt um Collection-Elemente durchzuiterieren.

![Stream](./img/stream_example.png)

*Stream Beispiel in [BubbleSort.java](src/main/java/nassois/geronimo/algorithms/BubbleSort.java)*

#### 3. JSON

Das JSON Datenformat war für dieses Projekt unabdingbar, da die Daten zu Darstellung der Algorithmen mehrere zehn- bis hundert-tausend Elemete beinhalten.

Der Server führt, nach der Auswahl des Sortier-Algorithmus sowie der jeweiligen Parameter, den jeweiligen Algorithmus aus.
Um den Algorithmus zu rekonstruieren und Darzustellen ist es notwendig, jeden Vergleich und Tausch die der Algorithmus durchführt zu speichern.
Die Datenstruktur ```SortingAlgorithmListener``` deckt diese Spezifikation ab.
In der Hilfs-Klasse [JSONParser.java](src/main/java/nassois/geronimo/utils/JSONParser.java) werden die Daten des Listeners zu einem JSON-Objekt umgewandelt und durch den Server versendet.

![JSON1](./img/JSON_1.png)

*Annotation ```@JsonInclude``` in der Klasse [SortingAlgorithmListener.java](src/main/java/nassois/geronimo/listener/SortingAlgorithmListener.java) versichert dass die Variablen im JSON enthalten sind*

![JSON1](./img/JSON_2.png)

*[JSONParser.java](src/main/java/nassois/geronimo/utils/JSONParser.java)*


## Quellennachweis

* [Inspiration der Projektstruktur] -
<https://github.com/Rudge/kotlin-javalin-realworld-example-app>
* [Funktionalität der Darstellung in Javascript] -
<https://p5js.org/B>
* [Quicksort-Algorithm] -
<https://www.baeldung.com/java-quicksort>
* [JSON-Parsing] -
<https://api.jquery.com/jQuery.parseJSON/>
* [Algorithmus-Darstellung] -
<https://thecodingtrain.com/CodingChallenges/143-quicksort.html>
* [Stepping-Stones] -
<https://stackoverflow.com/>



