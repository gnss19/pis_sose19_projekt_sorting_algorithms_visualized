import nassois.geronimo.algorithms.InsertionSort;
import nassois.geronimo.utils.RandomListGenerator;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class InsertionSortTest {
    @Test
    public void insertionSort_Test_0() {
        ArrayList<Integer> list = RandomListGenerator.generate(30);
        ArrayList<Integer> result = new InsertionSort().insertionSort(list, true);
        Integer expected [] = list.toArray(new Integer[list.size()-1]);
        Integer algorithmResult [] = result.toArray(new Integer[result.size()-1]);
        Assert.assertArrayEquals(expected, algorithmResult);
    }

    @Test
    public void insertionSort_Test_1() {
        ArrayList<Integer> list = RandomListGenerator.generate(300);
        ArrayList<Integer> result = new InsertionSort().insertionSort(list, true);
        Integer expected [] = list.toArray(new Integer[list.size()-1]);
        Integer algorithmResult [] = result.toArray(new Integer[result.size()-1]);
        Assert.assertArrayEquals(expected, algorithmResult);
    }

    @Test
    public void insertionSort_Test_2() {
        ArrayList<Integer> list = RandomListGenerator.generate(500);
        ArrayList<Integer> result = new InsertionSort().insertionSort(list, true);
        Integer expected [] = list.toArray(new Integer[list.size()-1]);
        Integer algorithmResult [] = result.toArray(new Integer[result.size()-1]);
        Assert.assertArrayEquals(expected, algorithmResult);
    }


    @Test
    public void insertionSort_Test_3() {
        ArrayList<Integer> list = RandomListGenerator.generate(123);
        ArrayList<Integer> result = new InsertionSort().insertionSort(list, true);
        Integer expected [] = list.toArray(new Integer[list.size()-1]);
        Integer algorithmResult [] = result.toArray(new Integer[result.size()-1]);
        Assert.assertArrayEquals(expected, algorithmResult);
    }


    @Test
    public void insertionSort_Test_4() {
        ArrayList<Integer> list = RandomListGenerator.generate(1);
        ArrayList<Integer> result = new InsertionSort().insertionSort(list, true);
        Integer expected [] = list.toArray(new Integer[list.size()-1]);
        Integer algorithmResult [] = result.toArray(new Integer[result.size()-1]);
        Assert.assertArrayEquals(expected, algorithmResult);
    }
}
