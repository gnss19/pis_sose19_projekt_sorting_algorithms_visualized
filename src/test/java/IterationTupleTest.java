import org.junit.Assert;
import org.junit.Test;
import nassois.geronimo.listener.IterationTuple;

public class IterationTupleTest {

    @Test
    public void iterationTuple_Test_0() {
        IterationTuple expected = new IterationTuple(1,2);
        IterationTuple actual = new IterationTuple(2,1);
        Assert.assertNotEquals(expected, actual);
    }

    @Test
    public void iterationTuple_Test_1() {
        IterationTuple expected = new IterationTuple(1,2, true);
        IterationTuple actual = new IterationTuple(2,1);
        Assert.assertNotEquals(expected, actual);
    }

    @Test
    public void iterationTuple_Test_2() {
        IterationTuple expected = new IterationTuple(1,2, true);
        IterationTuple actual = new IterationTuple(2,1, true);
        Assert.assertNotEquals(expected, actual);
    }

    @Test
    public void iterationTuple_Test_3() {
        IterationTuple expected = new IterationTuple(1,2, true);
        IterationTuple actual = new IterationTuple(1,2);
        Assert.assertNotEquals((IterationTuple) expected, actual);
    }

    @Test
    public void iterationTuple_Test_4() {
        IterationTuple expected = new IterationTuple(1,2);
        Assert.assertEquals(expected, expected);
    }

}

