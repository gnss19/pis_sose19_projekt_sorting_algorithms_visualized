import org.junit.Assert;
import org.junit.Test;
import nassois.geronimo.listener.SortingAlgorithmListener;
import nassois.geronimo.utils.RandomListGenerator;

import java.util.ArrayList;

public class SortingAlgorithmListenerTest {

    @Test
    public void sortinAlgorithmListener_Test_0() {
        ArrayList<Integer> list = RandomListGenerator.generate(30);
        SortingAlgorithmListener expected = new SortingAlgorithmListener();
        SortingAlgorithmListener actual = new SortingAlgorithmListener();
        expected.add(list);
        actual.add(list);
        Assert.assertEquals(expected.toString(), actual.toString());
    }

    @Test
    public void sortinAlgorithmListener_Test_1() {
        ArrayList<Integer> list = RandomListGenerator.generate(2000);
        SortingAlgorithmListener expected = new SortingAlgorithmListener();
        SortingAlgorithmListener actual = new SortingAlgorithmListener();
        expected.add(list);
        actual.add(list);
        Assert.assertEquals(expected.toString(), actual.toString());
    }

    @Test
    public void sortinAlgorithmListener_Test_2() {
        ArrayList<Integer> list = RandomListGenerator.generate(30);
        SortingAlgorithmListener expected = new SortingAlgorithmListener();
        SortingAlgorithmListener actual = new SortingAlgorithmListener();
        expected.add(list);
        list.add(2);
        actual.add(list);
        Assert.assertNotEquals(expected.toString(), actual.toString());
    }

    @Test
    public void sortinAlgorithmListener_Test_3() {
        SortingAlgorithmListener expected = new SortingAlgorithmListener();
        SortingAlgorithmListener actual = new SortingAlgorithmListener();
        Assert.assertEquals(expected.toString(), actual.toString());
    }

    @Test
    public void sortinAlgorithmListener_Test_4() {
        ArrayList<Integer> list = RandomListGenerator.generate(30);
        SortingAlgorithmListener expected = new SortingAlgorithmListener();
        SortingAlgorithmListener actual = new SortingAlgorithmListener();
        expected.add(list);
        list = RandomListGenerator.generate(31);
        actual.add(list);
        Assert.assertNotEquals(expected.toString(), actual.toString());
    }
}
