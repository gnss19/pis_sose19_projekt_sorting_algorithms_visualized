let drawThings = false;
let data = undefined;
let steps = undefined;
let canvasDiv = document.getElementById('sketch-holder');
let canvasWidth = canvasDiv.offsetWidth;
let canvasHeight = canvasDiv.offsetHeight;
let widthOfElem = undefined;
let mapHeigth = undefined;
let sketchCanvas;
let maxOflist = undefined;
let fr = 30;
let j = 0;
let stepIt = 0;
let comparisonsElem = $("#comparisons");
let reorderElem = $("#reorders");
let algorithmElem = $("#algorithm-information");

function updateFPS(value){
    fr = parseInt(value);
    frameRate(fr);
    comparisonsElem.html("Comparison: " + stepIt);
}

$(function () {
    $.get("/select-algorithm", function (json) {
        data = json;
        drawThings = false;
        steps = data.steps;
        widthOfElem = ((canvasWidth) / data.iterations[0].length) * 0.8;
        maxOflist = data.sortedList[(data.sortedList.length) - 1];
        mapHeigth = (canvasHeight - 80) / maxOflist;
        algorithmElem.html(data.algorithm);
        drawThings = true
        fpsCounter.html("FPS: " + fr);
    });
});

function setup() {
    frameRate(fr);
    sketchCanvas = createCanvas(canvasWidth, canvasHeight);
    sketchCanvas.parent("sketch-holder");
    background(255, 255, 255);
}

function draw() {
    fpsCounter = frameRate();
    if (drawThings) {
        fpsCounter.data = frameRate();
        background(255, 255, 255);
        if (data.iterations !== undefined) {
            for (let i = 0; i < data.iterations[j].length; i++) {
                if (steps[stepIt].pointerForSort == i) {
                    fill('#E0777D');
                    stroke(255, 0, 0);
                    strokeWeight(3);
                } else if (steps[stepIt].elemToBeSortet == i) {
                    fill('#D6FFB7');
                    stroke(0, 255, 0);
                    strokeWeight(5);
                } else {
                    fill(0, 0, 0);
                    stroke(0, 120, 120);
                    strokeWeight(1);
                }
                strokeWeight(1);
                rect(widthOfElem * i + 30, canvasHeight - 80, widthOfElem, (data.iterations[j][i]) * -mapHeigth);
            }
            if (steps[stepIt].swapped == true && j < data.iterations.length - 1) {
                j++;
            }
            if (stepIt < steps.length - 1) {
                stepIt++;
            }
            comparisonsElem.html("Comparison: " + stepIt);
            reorderElem.html("Reorders: " + j);
        }
    }
}
