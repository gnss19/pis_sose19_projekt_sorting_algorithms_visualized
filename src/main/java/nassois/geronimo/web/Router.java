package nassois.geronimo.web;

import io.javalin.Javalin;
import nassois.geronimo.web.controllers.AlgorithmController;
import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;

public class Router {

    public void register(Javalin app){
        AlgorithmController algorithmController = new AlgorithmController();

        //puts selected algorithm
        app.routes(() -> {
            path("/select-algorithm", () ->{
                algorithmController.choose(app);
                algorithmController.getResult(app);
            } );
            path("/sidenav-algorithms", () -> get(ctx -> ctx.redirect("https://en.wikipedia.org/wiki/Algorithm")));
            path("/sidenav-visualization", () -> get(ctx -> ctx.redirect("/")));
        });

    }
}
