package nassois.geronimo.web.controllers;

import io.javalin.Javalin;
import nassois.geronimo.algorithms.BubbleSort;
import nassois.geronimo.algorithms.InsertionSort;
import nassois.geronimo.algorithms.QuickSort;
import nassois.geronimo.algorithms.SelectionSort;
import nassois.geronimo.listener.SortingAlgorithmListener;
import nassois.geronimo.utils.JSONParser;
import nassois.geronimo.utils.RandomListGenerator;
import java.io.IOException;
import java.util.ArrayList;

public class AlgorithmController {

    ArrayList<Integer> list = new ArrayList<>();

    String JSONBody = "";
    int elementCount;

    public void choose(Javalin app){
        app.post("/select-algorithm", ctx -> {
            String algorithm = ctx.formParam("algorithms");
            elementCount = ctx.formParam("elements", Integer.class).getValue();
            JSONBody = compute(algorithm);
            ctx.redirect("/");
        });
    }

    public void getResult(Javalin app){
        app.get("/select-algorithm", ctx -> {
            ctx.contentType("application/json");
            ctx.result(JSONBody);
            JSONBody = "";
        });
    }

    private String compute(String algorithm) throws IOException {
        SortingAlgorithmListener sortingAlgorithmListener;
        list = null;
        list = RandomListGenerator.generate(elementCount);
        String result = new String();
        switch (algorithm){
            case "bubblesort":
                sortingAlgorithmListener = new BubbleSort().bubbleSort(list);
                result = JSONParser.getJSONfrom(sortingAlgorithmListener);
                return result;
            case "insertionsort":
                sortingAlgorithmListener = new InsertionSort().insertionSort(list);
                result = JSONParser.getJSONfrom(sortingAlgorithmListener);
                return result;
            case "selectionsort":
                sortingAlgorithmListener = new SelectionSort().selectionSort(list);
                result = JSONParser.getJSONfrom(sortingAlgorithmListener);
                return result;
            case "quicksort":
                sortingAlgorithmListener = new QuickSort().quickSort(list);
                result = JSONParser.getJSONfrom(sortingAlgorithmListener);
                return result;
            default:
                return result;
        }
    }
}
