package nassois.geronimo.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import nassois.geronimo.listener.SortingAlgorithmListener;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;

public abstract class JSONParser{

    public static String getJSONfrom(SortingAlgorithmListener dataObj) throws IOException{
        try{
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();
            mapper.writeValue(json, dataObj);
            String result = json.toString();
            return result;
        }catch (IOException e){
            System.out.println(e);
        }
        return "";
    }

    public static String getJSONfrom(ArrayList<Integer> dataObj) throws IOException{
        try {
            ObjectMapper mapper = new ObjectMapper();
            StringWriter json = new StringWriter();
            mapper.writeValue(json, dataObj);
            String result = json.toString();
            return result;
        }catch (IOException e){
            System.out.println(e);
        }
        return "";
    }
}
