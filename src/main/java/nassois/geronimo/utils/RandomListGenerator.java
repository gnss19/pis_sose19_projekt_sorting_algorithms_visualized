package nassois.geronimo.utils;

import java.util.ArrayList;
import java.util.Random;

public abstract class RandomListGenerator {

    public static ArrayList<Integer> generate(){
        return generate(20);
    }

    public static ArrayList<Integer> generate(int size){
        Random random = new Random();
        ArrayList<Integer> result = new ArrayList<>();
        for(int i = 1; i <= size; i++){
            result.add(random.nextInt(size*2));
        }
        return result;
    }
}
