package nassois.geronimo.listener;

import com.fasterxml.jackson.annotation.JsonInclude;

public class IterationTuple {

    @JsonInclude
    public final int elemToBeSortet, pointerForSort;
    @JsonInclude
    public final boolean swapped;

    public IterationTuple(int elemToBeSortet, int pointerForSort){
        this.elemToBeSortet = elemToBeSortet;
        this.pointerForSort = pointerForSort;
        this.swapped = false;
    }

    public IterationTuple(int elemToBeSortet, int pointerForSort, boolean swapped){
        this.elemToBeSortet = elemToBeSortet;
        this.pointerForSort = pointerForSort;
        this.swapped = true;
    }
}
