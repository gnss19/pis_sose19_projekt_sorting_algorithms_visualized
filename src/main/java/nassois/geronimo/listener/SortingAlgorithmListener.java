package nassois.geronimo.listener;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.eclipse.jetty.util.IO;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

public class SortingAlgorithmListener {

    @JsonInclude
    public ArrayList<ArrayList<Integer>> iterations = new ArrayList<>();

    @JsonInclude
    public ArrayList<IterationTuple> steps = new ArrayList<>();

    @JsonInclude
    public String algorithm = "";


    public SortingAlgorithmListener(String algorithm){
        this.algorithm = algorithm;
    }

    public SortingAlgorithmListener(){
    }

    public void add(IterationTuple step){
        steps.add(step);
    }

    public void add( ArrayList<Integer> list){
        iterations.add(new ArrayList<>(list));
    }

    public void add(ArrayList<Integer> list ,IterationTuple step){
        iterations.add(new ArrayList<>(list));
        steps.add(step);
    }

    public ArrayList<Integer> getSortedList(){
        return iterations.get(iterations.size()-1);
    }

    @Override
    public String toString(){
        ArrayList<ArrayList<Integer>> iterationsTS = new ArrayList<>(iterations);
        ArrayList<IterationTuple> stepsTS = new ArrayList<>(steps);
        Collections.reverse(iterationsTS);
        Collections.reverse(stepsTS);
        StringBuffer sb = new StringBuffer();
        Stack<ArrayList<Integer>> iterationQueue = new Stack<>();
        Stack<IterationTuple> stepQueue = new Stack<>();
        iterationsTS.forEach(e -> iterationQueue.push(e));
        stepsTS.forEach(e -> stepQueue.push(e));
        IterationTuple step;
        while(!iterationQueue.empty()){
            ArrayList<Integer> iteration = iterationQueue.pop();
            iteration.forEach(e -> sb.append(" " + e + " ,"));
            sb.append("\n");
            while(!stepQueue.empty()){
                step = stepQueue.pop();
                for(int i = 0; i < iteration.size(); i++){
                    if(step.elemToBeSortet == i){
                        sb.append(" . " + "|");
                    }else if(step.pointerForSort == i){
                        sb.append(" : " + "|");
                    }else {
                        sb.append("   |");
                    }
                }
                if(step.swapped){
                    sb.append("\n");
                    break;
                }
                sb.append("\n");
            }
        }
        return sb.toString();
    }
}
