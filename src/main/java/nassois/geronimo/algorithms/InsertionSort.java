package nassois.geronimo.algorithms;

import nassois.geronimo.listener.IterationTuple;
import nassois.geronimo.listener.SortingAlgorithmListener;
import java.util.ArrayList;
import java.util.Collections;

public class InsertionSort {

    SortingAlgorithmListener sortingAlgorithmListener = new SortingAlgorithmListener("Insertion-Sort");
    ArrayList<Integer> list;

    public ArrayList<Integer> insertionSort(ArrayList<Integer> transferredList, boolean test){
        insertionSort(transferredList);
        return list;
    }

    public SortingAlgorithmListener insertionSort(ArrayList<Integer> transferredList){
        sortingAlgorithmListener = new SortingAlgorithmListener("Insertion-Sort");
        list = transferredList;
        sortingAlgorithmListener.add(list);
        for(int i = 0; i < list.size(); i++){
            for(int j = i+1; j < list.size(); j++){
                if(list.get(i) > list.get(j)){
                    Collections.swap(list, i, j);
                    sortingAlgorithmListener.add(list, new IterationTuple(i,j,true));
                }
                else{
                    sortingAlgorithmListener.add(new IterationTuple(i,j));
                }
            }
        }
        return sortingAlgorithmListener;
    }

    public String getSortedResult(ArrayList<Integer> transferredList){
        insertionSort(transferredList);
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        list.forEach(e -> sb.append(e + "] + ["));
        return sb.toString();
    }
}
