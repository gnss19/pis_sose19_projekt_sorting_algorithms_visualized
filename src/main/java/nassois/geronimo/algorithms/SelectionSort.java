package nassois.geronimo.algorithms;

import nassois.geronimo.listener.IterationTuple;
import nassois.geronimo.listener.SortingAlgorithmListener;
import java.util.ArrayList;
import java.util.Collections;

public class SelectionSort {

    SortingAlgorithmListener sortingAlgorithmListener = new SortingAlgorithmListener("Selection Sort");
    ArrayList<Integer> list;

    public ArrayList<Integer> selectionSort(ArrayList<Integer> transferredList, boolean test){
        selectionSort(transferredList);
        return list;
    }

    public SortingAlgorithmListener selectionSort(ArrayList<Integer> transferredList){
        int i, j, min;
        sortingAlgorithmListener = new SortingAlgorithmListener("Selection Sort");
        list = transferredList;
        sortingAlgorithmListener.add(list);
        for(i = 0; i < list.size(); i++){
            min = i;
            sortingAlgorithmListener.add(new IterationTuple(min,min));
            for(j = i+1; j < list.size(); j++){
                sortingAlgorithmListener.add(new IterationTuple(min, j));
                min = (list.get(j) < list.get(min)) ? j : min;
            }
            if(min != i){
                Collections.swap(list, i, min);
                sortingAlgorithmListener.add(list, new IterationTuple(i, min, true));
            }
        }
        return sortingAlgorithmListener;
    }

    public String getSortedResult(ArrayList<Integer> transferredList){
        selectionSort(transferredList);
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        list.forEach(e -> sb.append(e + "] + ["));
        return sb.toString();
    }
}
