package nassois.geronimo.algorithms;

import nassois.geronimo.listener.IterationTuple;
import nassois.geronimo.listener.SortingAlgorithmListener;
import java.util.ArrayList;
import java.util.Collections;

public class BubbleSort {

    SortingAlgorithmListener sortingAlgorithmListener = new SortingAlgorithmListener("Bubble-Sort");
    ArrayList<Integer> list;

    public ArrayList<Integer> bubbleSort(ArrayList<Integer> list, boolean test){
        bubbleSort(list);
        return list;
    }

    public SortingAlgorithmListener bubbleSort(ArrayList<Integer> transferredList){
        sortingAlgorithmListener = new SortingAlgorithmListener("Bubble-Sort");
        list = transferredList;
        int length = list.size();
        int temp = 0;
        sortingAlgorithmListener.add(list);
        for(int i = 0; i < length-1; i++){
            for(int j = 0; j < (length-i-1); j++){
                if(list.get(j) > list.get(j+1)){
                    Collections.swap(list, j+1, j);
                    sortingAlgorithmListener.add(list, new IterationTuple(j, j+1, true));
                    continue;
                }
                sortingAlgorithmListener.add(new IterationTuple(j, j+1));
            }
        }
        return sortingAlgorithmListener;
    }

    public String getSortedResult(ArrayList<Integer> transferredList){
        bubbleSort(transferredList);
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        list.forEach(e -> sb.append(e + "] + ["));
        return sb.toString();
    }
}
