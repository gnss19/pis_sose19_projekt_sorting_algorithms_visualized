package nassois.geronimo.algorithms;

import nassois.geronimo.listener.IterationTuple;
import nassois.geronimo.listener.SortingAlgorithmListener;
import java.util.ArrayList;
import java.util.Collections;


public class QuickSort {

    SortingAlgorithmListener sortingAlgorithmListener = new SortingAlgorithmListener("Quick-Sort");
    ArrayList<Integer> list;

    public ArrayList<Integer> quickSort(ArrayList<Integer> transferredList, boolean test) {
        quickSort(transferredList);
        return list;
    }


    public SortingAlgorithmListener quickSort(ArrayList<Integer> transferredList) {
        sortingAlgorithmListener = new SortingAlgorithmListener("Quick-Sort");
        list = transferredList;
        int begin = 0;
        int end = list.size()-1;
        if (begin < end) {
            int partitionIndex = partition(list, begin, end, sortingAlgorithmListener);
            quickSort(list, begin, partitionIndex-1, sortingAlgorithmListener);
            quickSort(list, partitionIndex+1, end, sortingAlgorithmListener);
        }
        return sortingAlgorithmListener;
    }

    public void quickSort(ArrayList<Integer> list, int begin, int end, SortingAlgorithmListener sortingAlgorithmListener) {
        if (begin < end) {
            int partitionIndex = partition(list, begin, end, sortingAlgorithmListener);
            quickSort(list, begin, partitionIndex-1, sortingAlgorithmListener);
            quickSort(list, partitionIndex+1, end, sortingAlgorithmListener);
        }
    }

    private int partition(ArrayList<Integer> list, int begin, int end, SortingAlgorithmListener sortingAlgorithmListener) {
        int pivot = list.get(end);
        int i = (begin-1);

        for (int j = begin; j < end; j++) {
            sortingAlgorithmListener.add(new IterationTuple(i,end));
            if (list.get(j) <= pivot) {
                i++;
                Collections.swap(list, i,j);
                sortingAlgorithmListener.add(list, new IterationTuple(i,j,true));
            }
        }

        Collections.swap(list, i+1, end);
        sortingAlgorithmListener.add(list, new IterationTuple(i+1,end,true));
        return i+1;
    }

    public String getSortedResult(ArrayList<Integer> transferredList){
        quickSort(transferredList);
        StringBuffer sb = new StringBuffer();
        sb.append("[");
        list.forEach(e -> sb.append(e + "] + ["));
        return sb.toString();
    }
}
