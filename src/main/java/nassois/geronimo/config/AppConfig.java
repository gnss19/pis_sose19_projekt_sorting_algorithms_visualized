package nassois.geronimo.config;

import io.javalin.Javalin;
import nassois.geronimo.web.Router;
import nassois.geronimo.web.controllers.AlgorithmController;

public abstract class AppConfig {

    private static AlgorithmController algorithmController = new AlgorithmController();
    private static Router router = new Router();

    public static Javalin setup(){
        Javalin app = Javalin.create()
                .enableStaticFiles("/public")
                .ws("/", wsHandler -> {
                    wsHandler.onMessage(((session, msg) -> {
                        session.send(msg);
                    }));
                });
        router.register(app);
        return app;
    }
}
