package nassois.geronimo;

import nassois.geronimo.config.AppConfig;

public class App {
    public static void main(String[] args) {
        AppConfig.setup().start();
    }
}